package com.example.s.exercise2.newsList;

import android.app.ActionBar;
import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.s.exercise2.R;
import com.example.s.exercise2.utils.DataUtils;


public class NewsRecyclerActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_recycler);


        /*ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }*/

        RecyclerView list = findViewById(R.id.recycler);
        list.setAdapter(new NewsAdapter(this, DataUtils.generateNews()));
        Configuration orientation = new Configuration();
        if(list.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
        list.setLayoutManager(new GridLayoutManager(this,1));}
        else if(list.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            list.setLayoutManager(new GridLayoutManager(this,2));
        }

        }
}
