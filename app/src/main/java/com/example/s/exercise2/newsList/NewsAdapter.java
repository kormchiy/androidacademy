package com.example.s.exercise2.newsList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;
import com.example.s.exercise2.R;
import com.example.s.exercise2.utils.Category;
import com.example.s.exercise2.utils.NewsItem;

import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsHolder> {
    @NonNull
    private final List<NewsItem> news;
    @NonNull
    private final LayoutInflater inflater;
    @NonNull
    private final RequestManager imageLoader;



    public NewsAdapter(NewsRecyclerActivity newsRecyclerActivity, List<NewsItem> newsItems) {
        this.news = newsItems;
        this.inflater = LayoutInflater.from(newsRecyclerActivity);



        RequestOptions imageOption = new RequestOptions()
                .placeholder(R.drawable.image_placeholder)
                .fallback(R.drawable.image_placeholder)
                .centerCrop();
        this.imageLoader = Glide.with(newsRecyclerActivity).applyDefaultRequestOptions(imageOption);
    }


    @NonNull
    @Override
    public NewsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NewsHolder(inflater.inflate(R.layout.activity_news_item, parent, false));
    }



    @Override
    public void onBindViewHolder(@NonNull NewsHolder viewHolder, int position) {
        viewHolder.bind(news.get(position));
    }


    @Override
    public int getItemCount() {
        return news.size();
    }


    class NewsHolder extends RecyclerView.ViewHolder {
        private final ImageView imageView;
        private final TextView category;
        private final TextView headLine;
        private final TextView line;
        private final TextView date;

        NewsHolder(@NonNull View itemView) {
            super(itemView);
         /*   itemView.setOnClickListener(view ->{
                Intent intent = new Intent(, AboutNewsActivity.class);
                startActivity(intent);
            });*/

            imageView = itemView.findViewById(R.id.imageNews);
            category = itemView.findViewById(R.id.category);
            headLine = itemView.findViewById(R.id.headLine);
            line = itemView.findViewById(R.id.line);
            date = itemView.findViewById(R.id.date);
        }

        void bind(NewsItem news) {
            imageLoader.load(news.getImageUrl()).into(imageView);
            category.setText(news.getCategory().getName());
            headLine.setText(news.getTitle());
            line.setText(news.getPreviewText());
            date.setText(news.getPublishDate().toString());
        }

    }
}