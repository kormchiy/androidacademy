package com.example.s.exercise2;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity implements View.OnClickListener {

    EditText editText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button buttonToProject = findViewById(R.id.button);
        buttonToProject.setOnClickListener(this);

        editText = findViewById(R.id.editMessage);
    }


    @Override
    public void onClick(View v) {
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + "aaa"));
        if(editText.length()!=0){
        intent.putExtra(Intent.EXTRA_TEXT, editText.getText().toString());
        intent.putExtra(Intent.EXTRA_SUBJECT, editText.getText().toString());

        if (intent.resolveActivity(getPackageManager()) != null){
        startActivity(intent);

    }}
        else Toast.makeText(getApplicationContext(),"введите текст",Toast.LENGTH_LONG).show();


    }


}
